<?php
//
// inbox.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//

require_once 'include/common.php';
bb_init();
require_once 'include/auth.php';
require_once 'include/database.php';
$db_handle = db_connect();
$user_array = validate_login($db_handle);

if($user_array['uid'] === "0")
{
	header("Location: login.php");
	exit;
}
$action = "";
if(isset($_GET['action']) && is_string($_GET['action']))
	$action = $_GET['action'];
//POST gets priority
if(isset($_POST['action']) && is_string($_POST['action']))
	$action = $_POST['action'];

if($action === "view")
{
	require_once 'include/viewpm.php';
	viewpm($db_handle, $user_array);
}
elseif($action === "new")
{
	require_once 'include/newpm.php';
	newpm($db_handle, $user_array);
}
elseif($action === "Mark as read")
{
	require_once 'include/pmmod.php';
	pm_markasread($db_handle, $user_array);
}
elseif($action === "Mark as unread")
{
	require_once 'include/pmmod.php';
	pm_markasunread($db_handle, $user_array);
}
elseif($action === "Mark all as read")
{
	require_once 'include/pmmod.php';
	pm_markallasread($db_handle, $user_array);
}
elseif($action === "Delete")
{
	require_once 'include/pmmod.php';
	pm_delete($db_handle, $user_array);
}
elseif($action === "Delete everything")
{
	require_once 'include/pmmod.php';
	pm_deleteall($db_handle, $user_array);
}
else
{
	if($_SERVER['REQUEST_METHOD'] !== "GET")
	{
		header("Location: " . $_SERVER['REQUEST_URI']);
		exit;
	}
	require_once 'include/viewinbox.php';
	viewinbox($db_handle, $user_array);
	db_unsetuser_alerts($db_handle, $user_array['uid']);
}
?>
