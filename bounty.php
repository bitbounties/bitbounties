<?php
//
// bounty.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
require_once 'include/common.php';
require_once 'include/auth.php';
require_once 'include/database.php';
bb_init();
if(isset($_GET["id"]) == false || !is_numeric($_GET["id"]))
{
	header("Location: browse.php");
	exit;
}

$db_handle = db_connect();
$user_array = validate_login($db_handle);
if(isset($_GET['action']) && $_GET['action'] === "comment")
{
	require_once 'include/comment.php';
	post_comment($db_handle, $user_array);
}
else
{
	require_once 'include/viewbounty.php';
	viewbounty($db_handle, $user_array);
}
?>
