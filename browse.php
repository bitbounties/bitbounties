<?php 
//
// browse.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//

require_once 'include/common.php';
bb_init();
require_once 'include/auth.php';
require_once 'include/database.php';
$db_handle = db_connect();
$user_array = validate_login($db_handle);

$bounties_list = browse_search($db_handle, $user_array);

display_header($user_array, "browse");
if($bounties_list[0] != "0")
{
	$trusted_resultrows = create_bountyrows($db_handle, $bounties_list);
	$trusted_browsepages_block = create_browsepages($bounties_list[0]);
	require docroot() . 'include/template/t_browse_all.php';
}
else
{
	$trusted_errormsg = '<div class="common-box" style="color: #777">No results found</div>';
	require docroot() . 'include/template/t_browse_error.php';
}
display_footer($user_array);

//-------------------
function browse_search($db_handle, $user_array)
{
	//***************************
	//$category_array
	//$status_array
	$searchstr = "";
	//order
	$category_array = array();
	$status_array = array();
	if(isset($_GET["cat1"]) && $_GET["cat1"] === "1")
		array_push($category_array, 1);
	if(isset($_GET["cat2"]) && $_GET["cat2"] === "1")
		array_push($category_array, 2);
	if(isset($_GET["cat3"]) && $_GET["cat3"] === "1")
		array_push($category_array, 3);
	if(isset($_GET["cat4"]) && $_GET["cat4"] === "1")
		array_push($category_array, 4);
	if(isset($_GET["open"]) && $_GET["open"] === "1")
		array_push($status_array, "open");
	if(isset($_GET["closed"]) && $_GET["closed"] === "1")
		array_push($status_array, "closed");
	if(isset($_GET["filled"]) && $_GET["filled"] === "1")
		array_push($status_array, "filled");
	if(isset($_GET["pending"]) && $_GET["pending"] === "1")
		array_push($status_array, "pending");
	if(isset($_GET["search"]) && $_GET["search"] !== "")
		$searchstr = trim($_GET["search"]);
	if(isset($_GET["page"]) && $_GET["page"] > 0)
		$curpage = (int) $_GET["page"];
	else
		$curpage = 1;
	$bounties_list = db_getbountylist($db_handle, ($curpage - 1) * get_bounties_perpage(),
									  get_bounties_perpage(), $category_array, $status_array, 1, $searchstr, "", "");
	return $bounties_list;
}

function create_browsepages($total_results)
{
	$pages = ceil($total_results / get_bounties_perpage());
	$pagegets = "";
	if($pages == 1)
		return "";
	foreach($_GET as $index => $data)
	{
		if(is_string($data) && $index !== "page")
			$pagegets .= htmlspecialchars("&{$index}={$data}");
	}

	if(isset($_GET["page"]) && $_GET["page"] > 0)
		$curpage = (int) $_GET["page"];
	else
		$curpage = 1;
	$pages_division = "<div>";
	if($curpage - 2 > 1)
		$pages_division .= '<a href="browse.php?page=1' . $pagegets . '">first</a>&nbsp;';
	if($curpage - 2 >= 1)
		$pages_division .= '<a href="browse.php?page=' . ($curpage - 2) . $pagegets . '">' . ($curpage - 2) . '</a>&nbsp;';
	if($curpage - 1 >= 1)
		$pages_division .= '<a href="browse.php?page=' . ($curpage - 1) . $pagegets . '">' . ($curpage - 1) . '</a>&nbsp;';

	$pages_division .= $curpage . "&nbsp;";

	if($curpage + 1 <= $pages)
		$pages_division .= '<a href="browse.php?page=' . ($curpage + 1) . $pagegets . '">' . ($curpage + 1) .'</a>&nbsp;';
	if($curpage + 2 <= $pages)
		$pages_division .= '<a href="browse.php?page=' . ($curpage + 2) . $pagegets . '">' . ($curpage + 2) . '</a>&nbsp;';
	if($curpage + 2 < $pages)
		$pages_division .= '<a href="browse.php?page=' . $pages . $pagegets . '">last</a>';
	$pages_division .= "</div>";
	return $pages_division;
}

function create_bountyrows($db_handle, $bounties_list)
{
	$total_results = $bounties_list[0];
	$count = count($bounties_list) - 1;
	$return_array = array($count);
	for($i = 1; $i <= $count; $i++)
	{
		$trusted_row = array();
		$trusted_row['bid'] = (int)$bounties_list[$i]['id'];
		$trusted_row['title'] = htmlspecialchars($bounties_list[$i]['title']);
		$trusted_row['uid'] = (int)$bounties_list[$i]['creator'];
		$trusted_row['username'] = htmlspecialchars(db_getusername_byuid($db_handle, $bounties_list[$i]['creator']));
		$trusted_row['catname'] = htmlspecialchars(db_getcategory_byid($db_handle, $bounties_list[$i]['category'])['name']);
		$trusted_row['createdate'] = formattime($bounties_list[$i]['createdate']);
		$trusted_row['votes'] = (int)$bounties_list[$i]['votes'];
		$trusted_row['comments'] = (int)$bounties_list[$i]['comments'];
		$trusted_row['btc'] = "";
		$trusted_row['ltc'] = "";
		$trusted_row['bl'] = "";
		$trusted_row['br'] = "";

		switch($bounties_list[$i]['status'])
		{
			case "open":
				$trusted_row['status'] = "open";
				break;
			case "claimed, pending":
				$trusted_row['status'] = "pending";
				break;
			case "claimed, paid":
				$trusted_row['status'] = "filled";
				break;
			default:
				$trusted_row['status'] = "closed";
		}

		if($bounties_list[$i]['satoshi'] != "0")
			$trusted_row['btc'] = formatbtc($bounties_list[$i]['satoshi']);
		if($bounties_list[$i]['litoshi'] != "0")
			$trusted_row['ltc'] = formatltc($bounties_list[$i]['litoshi']);

		if($i == $count)
		{
			//they must start with a space
			$trusted_row['bl'] = " bl";
			$trusted_row['br'] = " br";
		}
		array_push($return_array, $trusted_row);
	}
	return $return_array;

}
?>
