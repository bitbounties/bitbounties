<?php
//
// error.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
include_once 'include/bb_config.php';

if(!isset($_GET['id']))
{
	header("Location: index.php");
	exit;
}
if(!defined('BB_MAINTENANCE'))
{
	if($_GET['id'] !== "999")
	{
		header("Location: error.php?id=999");
		exit;
	}
}
elseif(BB_SECURITY_MAINTENANCE === 1)
{
	if($_GET['id'] !== "1000")
	{
		header("Location: error.php?id=1000");
		exit;
	}
}
elseif(BB_MAINTENANCE == 1)
{
	if($_GET['id'] !== "0")
	{
		header("Location: error.php?id=0");
		exit;
	}
}

$reloadlink = '[<a href="index.php">RELOAD</a>]';
if($_GET["id"] === "0")
{
	if(BB_MAINTENANCE === 0)
		header("Location: index.php");
	$title = "MAINTENANCE";
	$icon = "...";
	$msg = "A scheduled maintenance is in progress.<br>This usually means we are performing a backup or upgrading something.<br>Sorry for any inconvenience";
}
elseif($_GET["id"] >= 1 && $_GET["id"] <= 20)
{
	$title = "ERROR";
	$icon = "!";
	$msg = "Internal server error.<br> Please contact the administrator &amp; provide the following error id: " . htmlspecialchars($_GET["id"]);
}
elseif($_GET["id"] === "999")
{
	if(defined('BB_MAINTENANCE'))
		header("Location: index.php");
	$title = "ERROR";
	$icon = "!";
	$msg = "There is an error with server configuration.<br>Please contact the administrator.";
}
elseif($_GET["id"] === "1000")
{
	if(BB_SECURITY_MAINTENANCE === 0)
		header("Location: index.php");
	$title = "SECURITY MAINTENANCE";
	$icon = "!!";
	$msg = "Website is down for security reasons.<br>This could mean that the we detected an attack.<br>We don't know if your coins are safe yet";
}
elseif($_GET["id"] === "1001")
{
	$title = "COMMENT";
	$icon = ".";
	$msg = "There was an error submitting your comment.<br>Press back button on your browser and try again.";
	$reloadlink = "";
}
elseif($_GET["id"] === "1002")
{
	$title = "COMMENT";
	$icon = ".";
	$msg = "Your comment seems to be empty.<br>Press back button on your browser and try again.";
	$reloadlink = "";
}
else
	header("Location: index.php");
?>
<!doctype html>
<html>
<head>
<title>bitbounty</title>
<meta charset="UTF-8" />
<link href="res/layout.css" rel='stylesheet' type='text/css' />
</head>
<body>
	<nav class="head-nav">
		<ul class="head-nav">
			<li class="nav"><a class="nav" id="active"><?php echo $title;?> </a>
			</li>
		</ul>
	</nav>
	<div class="content">
		<div style="font-size: 15em">
			<?php echo $icon; ?>
		</div>
		<h1>
			<?php echo $title; ?>
		</h1>
		<p>
			<?php echo $msg; ?>
		</p>
		<?php echo $reloadlink; ?>
	</div>

</body>
</html>
