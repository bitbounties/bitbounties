<?php
//
// new.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
require_once 'include/common.php';
bb_init();
require_once 'include/auth.php';
require_once 'include/database.php';
$db_handle = db_connect();
$user_array = validate_login($db_handle);
display_header($user_array, "new");
?>
<div class="content">
	<?php echo "<h1 style='text-align: center;'>New Bounty</h1>";?>
</div>
<?php display_footer($user_array);?>
