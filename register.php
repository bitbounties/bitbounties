<?php 
//
// register.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
require_once 'include/common.php';
bb_init();
require_once 'include/auth.php';
require_once 'include/database.php';
$db_handle = db_connect();
$user_array = validate_login($db_handle);
if($user_array["uid"] != "0")
{
	header("Location: index.php");
	exit;
}
$untrusted_username = "";
$untrusted_pubkey = "";
if(isset($_POST["submit"]))
{
	$register_msg = '';
	if(isset($_POST["user"]) && $_POST["user"] !== "")
	{
		$untrusted_username = $_POST["user"];
	}
	else
		$register_msg .= "<li>Username is empty</li>";

	if(isset($_POST["pass"]) && $_POST["pass"] !== "" && isset($_POST["rpass"]) && $_POST["rpass"] !== "")
	{
		$untrusted_password = $_POST["pass"];
		$untrusted_repassword = $_POST["rpass"];
		if($untrusted_password !== $untrusted_repassword)
			$register_msg .= "<li>Passwords didn't match</li>";
		else
		{
			$password_hash = password_hash($untrusted_password, PASSWORD_DEFAULT);
		}
	}
	else
		$register_msg .= "<li>Password fields cannot be empty</li>";

	if(isset($_POST["pubkey"]) && $_POST["pubkey"] !== "")
	{
		$untrusted_pubkey = $_POST["pubkey"];
	}
	else
		$register_msg .= "<li>Verification Address is required</li>";
}
if(bb_register_enabled() == 0)
	$register_msg = "Registeration is disabled by administrator.";

display_header($user_array, "register");
?>
<div class="content">
	<h1>Register a new account</h1>
	<?php 
	if(isset($register_msg) && $register_msg !== '')
		echo '<ul id="error-msg">' . $register_msg . '</ul>';
	if(bb_register_enabled() == 0)
	{
		display_footer($user_array);
		exit;
	}
	?>
	<form action="register.php" method="post" class="common-box">
		<table class="form-table right">
			<tr>
				<td><label for="user">Username</label></td>
				<td><input class="common-textbox" type="text" id="user"
					value="<?php echo htmlspecialchars($untrusted_username);?>"
					name="user"></td>
			</tr>
			<tr>
				<td><label for="pass">Password</label></td>
				<td><input class="common-textbox" type="password" id="pass" name="pass">
				</td>
			</tr>
			<tr>
				<td><label for="rpass">Repeat Password</label></td>
				<td><input class="common-textbox" type="password" id="rpass"
					name="rpass"></td>
			</tr>
			<tr>
				<td><label for="pubkey">Verification Address</label></td>
				<td><input class="common-textbox" type="text" id="pubkey"
					value="<?php echo htmlspecialchars($untrusted_pubkey);?>"
					name="pubkey"></td>
			</tr>
		</table>
		<input style="margin-top: 20px" type="submit" value="Register"
			name="submit">
	</form>
</div>

<?php display_footer($user_array);?>
