<?php 
//
// login.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
require_once 'include/common.php';
bb_init();
require_once 'include/auth.php';
require_once 'include/database.php';
$db_handle = db_connect();
$user_array = validate_login($db_handle);
$bb_page="login";
$untrusted_username = "";
$untrusted_password = "";
$untrusted_remember = "";
if(isset($_GET["logout"]) && $_GET["logout"] === "1")
{
	session_destroy();
	setcookie("uid", null, 0);
	setcookie("pwd", null, 0);
	if(isset($user_array["uid"]) && $user_array["uid"] !== "0")
		db_unsetuser_cookieval($db_handle, $user_array["uid"]);
	header("Location: index.php");
	exit;
}
if($user_array["uid"] !== "0")
{
	header("Location: index.php");
	exit;
}
if(isset($_POST["submit"]) == true)
{
	if(isset($_POST["user"])) $untrusted_username = $_POST["user"];
	if(isset($_POST["pass"])) $untrusted_password = $_POST["pass"];
	if(isset($_POST["remember"])) $untrusted_remember = $_POST["remember"];
	if($untrusted_username == "" || $untrusted_password == "")
		$login_msg = "<p id='error-msg'>Error: Username or Password field cannot be empty.</p>";
	else
	{
		$user_iuplsla = check_userpw($db_handle, $untrusted_username, $untrusted_password);
		$uid = $user_iuplsla[0];
		if($uid >= 1)
		{
			new_session($db_handle, $uid);
			if($untrusted_remember === "remember")
			{
				$cookieval = authorize_cookie($db_handle, $uid);
				setcookie("uid", $uid, 7258105800);
				setcookie("pwd", $cookieval, 7258105800);
			}
			header("Location: index.php");
			exit;
		}
		elseif($uid == 0)
			$login_msg = "<p id='error-msg'>Error: Invalid username or password.";
		else
			$login_msg = "<p id='error-msg'>Error: Your account is disabled.</p>";
	}
}
display_header($user_array, "login");
?>
<div class="content">
	<h1>Login</h1>
	<?php	if(isset($login_msg))
		echo $login_msg;
	?>
	<form action="login.php" method="post" class="common-box">
		<table class="form-table right">
			<tr>
				<td><label for="user">Username</label></td>
				<td><input class="common-textbox"
					value="<?php echo htmlspecialchars($untrusted_username); ?>"
					type="text" id="user" name="user" required></td>
			</tr>
			<tr>
				<td><label for="pass">Password</label></td>
				<td><input class="common-textbox" type="password" id="pass" name="pass" required>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="checkbox" id="remember" value="remember"
					name="remember"
					<?php if($untrusted_remember === "remember") echo "checked";
					?>><label for="remember">Remember</label> <input type="submit"
					value="LOGIN" name="submit"></td>
			</tr>
		</table>
	</form>
</div>
<?php display_footer($user_array);?>
