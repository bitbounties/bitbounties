# bitbounties #

This is the sourcetree for bitbounties.org website.

bitbounties is under active development and is not ready for production yet.

Feel free to browse the code and submit pull requests, bug reports etc.

### License ###

Every file is licensed as AGPLv3 or later, unless explicitly stated.

See COPYING for more details
