<?php
//
// newpm.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//

function get_trustedpmlist()
{
	$trusted_pm = array();

	$trusted_pm['recipient'] = "";
	$trusted_pm['subject'] = "";
	$trusted_pm['text'] = "";

	if(isset($_POST['recipient']) && is_string($_POST['recipient']))
		$trusted_pm['recipient'] = trim($_POST['recipient']);

	if(isset($_POST['subject']) && is_string($_POST['subject']))
		$trusted_pm['subject'] = trim($_POST['subject']);

	if(isset($_POST['text']) && is_string($_POST['text']))
		$trusted_pm['text'] = trim($_POST['text']);
	if(isset($_POST['mode']) && $_POST['mode'] === "uid")
		$trusted_pm['mode'] = "uid";
	else
		$trusted_pm['mode'] = "username";
	if(!empty($_GET['id']) && is_numeric($_GET['id']))
	{
		$trusted_pm['recipient'] = (int) $_GET['id'];
		$trusted_pm['mode'] = "uid";
	}
	return $trusted_pm;
}

function display_newpmform($user_array, $errormsg_block, $trusted_pm)
{
	$root = $_SERVER['DOCUMENT_ROOT'];
	display_header($user_array, "inbox");

	$template = file_get_contents($root . "include/template/t_newpm_all.php");
	echo preg_replace_callback('/\{trusted_pm_(.*)\}/U',
		function ($match) use ($trusted_pm, $errormsg_block)
		{
			if($match[1] !== "msg")
				return htmlspecialchars($trusted_pm[$match[1]]);
			return $errormsg_block;
		}, $template);
	display_footer($user_array);
}

function display_newpm_success($user_array)
{
	$root = $_SERVER['DOCUMENT_ROOT'];
	display_header($user_array, "inbox");
	$template = file_get_contents($root . "include/template/t_common_error.php");
	echo preg_replace_callback('/\{trusted_common_(.*)\}/U',
		function ($match)
		{
			if($match[1] === "header")
				return "Send PM";
			elseif($match[1] === "errormsg")
				return "Message sent successfully.";
			elseif($match[1] === "url")
				return "inbox.php";
			elseif($match[1] === "urltitle")
				return "Return to inbox";
			return "";
		}, $template);
	display_footer($user_array);
}

function sendpm($db_handle, $sender_user_array, $trusted_pm)
{
	if($trusted_pm['recipient'] === "" || $trusted_pm['subject'] === "" || $trusted_pm['text'] === "")
	   return 1;

	if($sender_user_array['uid'] < 1 || $sender_user_array['acclevel'] < 1)
		return 2;

	if($trusted_pm['mode'] === "uid")
		$receiver_user_array = db_getuser_byuid_assoc($db_handle, $trusted_pm['recipient']);
	else
		$receiver_user_array = db_getuser_byname_assoc($db_handle, $trusted_pm['recipient']);

	if($receiver_user_array['uid'] === "0")
		return 3;
	if($receiver_user_array['acclevel'] < 1)
		return 4;
	if($receiver_user_array['uid'] === $sender_user_array['uid'])
		return 5;
	if(db_sendpm($db_handle, $sender_user_array['uid'], $receiver_user_array['uid'],
				 $_SERVER['REMOTE_ADDR'], $trusted_pm['subject'], $trusted_pm['text']) == 1)
		return 0;
	return -1;
}

function newpm($db_handle, $user_array)
{
	$errormsg_block = "";
	$status = 1;
	$trusted_pmlist = get_trustedpmlist();
	if(!empty($_POST['submit']))
	{
		$status = sendpm($db_handle, $user_array, $trusted_pmlist);
		switch($status)
		{
			case 1:
				$errormsg = "All fields are required.";
				break;
			case 2:
				$errormsg = "You must login before sending pm.";
				break;
			case 3:
				$errormsg = "Could not find the specified recipient.";
				break;
			case 4:
				$errormsg = "Recipient's account is disabled.";
				break;
			case 5:
				$errormsg = "You can't PM yourself!";
				break;
			default:
				$errormsg = "Unhandled error. Please try again. If this message appears again, contact the administrator.";
		}
	}
	if(!empty($errormsg))
		$errormsg_block = "<p id='error-msg'>{$errormsg}</p>";
	if(!$status)
		display_newpm_success($user_array);
	else
		display_newpmform($user_array, $errormsg_block, $trusted_pmlist);
}
?>
