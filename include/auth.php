<?php
//
// auth.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
function check_userpw($db_handle, $untrusted_user, $untrusted_password)
{
	$user_iuplsla = db_getuser_byname($db_handle, $untrusted_user);
	if($user_iuplsla[0] >= 1)
	{
		if(password_verify($untrusted_password, $user_iuplsla[2]))
		{
			if($user_iuplsla[3] >= 1)
				return $user_iuplsla;
			else
				return array(0 => "-1");
		}
	}
	return array(0 => "0");
}

function authorize_cookie($db_handle, $untrusted_uid)
{
	$cookieval = bin2hex(openssl_random_pseudo_bytes(16));
	$hashedcookieval = password_hash($cookieval, PASSWORD_DEFAULT);
	db_setuser_cookieval($db_handle, $untrusted_uid, $hashedcookieval);
	return $cookieval;
}

function check_cookie($db_handle, $untrusted_uid, $cookieval)
{
	$user_iuclsla = db_getuser_byuid($db_handle, $untrusted_uid);
	if(password_verify($cookieval, $user_iuclsla[2]) && $user_iuclsla[3] >= 1)
	{
		new_session($db_handle, (int)$untrusted_uid);
		return $user_iuclsla;
	}
	setcookie("uid", null, 1);
	setcookie("pwd", null, 1);
	db_unsetuser_cookieval($db_handle, $untrusted_uid);
	return array(0 => "0");
}

function check_session($db_handle)
{
	if(isset($_SESSION["uid"]))
	{
		if($_SESSION["uid"] >= 1 && $_SESSION["ip"] === $_SERVER["REMOTE_ADDR"] && $_SESSION['lastseen'] > time() - 1800)
		{
			$_SESSION['lastseen'] = time();
			$user_iuclsla = db_getuser_byuid($db_handle, $_SESSION["uid"]);
			if($user_iuclsla[0] >= 1 && $user_iuclsla[3] > 0)
				return $user_iuclsla;
		}
	}
	return array(0 => "0");
}
function new_session($db_handle, $uid)
{
	session_destroy();
	session_start();
	$_SESSION["uid"] = $uid;
	$_SESSION["ip"] = $_SERVER["REMOTE_ADDR"];
	$_SESSION["lastseen"] = time();
	db_setuser_lastlogin($db_handle, $uid, $_SERVER["REMOTE_ADDR"]);
}
function validate_login($db_handle)
{
	$user_iuclsla = check_session($db_handle);
	if($user_iuclsla[0] == "0" && isset($_COOKIE["uid"]) && isset($_COOKIE["pwd"]))
		$user_iuclsla = check_cookie($db_handle, $_COOKIE["uid"], $_COOKIE["pwd"]);

	if($user_iuclsla[0] === "0")
	{
		setcookie("uid", null, 0);
		setcookie("pwd", null, 0);
		session_destroy();
		return array("uid" => "0");
	}
	return array("uid" => $user_iuclsla[0], "username" => $user_iuclsla[1],
			"acclevel" => $user_iuclsla[3], "satoshi" => $user_iuclsla[4], "litoshi" => $user_iuclsla[5], "alerts" => $user_iuclsla[6]);
}
?>
