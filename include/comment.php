<?php
//
// comment.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//

require_once 'common.php';

function get_trustedcommentlist($db_handle, $trusted_bountyid, $user_acclevel)
{
	
	if(!isset($_GET["commentpage"]) || !is_numeric($_GET["commentpage"]) || $_GET["commentpage"] < 0)
		$cur_page = 1;
	else
		$cur_page = (int)$_GET["commentpage"];
	if($user_acclevel > 2)
		$show_invisible = 1;
	else
		$show_invisible = 0;
	$perpage = get_comments_perpage();
	$comment_list = db_getcommentlist_bybounty($db_handle, $trusted_bountyid, ($cur_page - 1) * $perpage, $perpage, $show_invisible);
	if($comment_list[0] === "0")
		return array("0");
	$count = count($comment_list) - 1;
	$return_array = array($comment_list[0]);
	for($i = 1; $i <= $count; $i++)
	{
		$trusted_cid = (int)$comment_list[$i]["id"];
		$trusted_uid =  (int)$comment_list[$i]["uid"];
		$trusted_username = htmlspecialchars($comment_list[$i]["username"]);
		$trusted_createdate = formattime($comment_list[$i]["created"]);
		$trusted_acclevel = (int)$comment_list[$i]["acclevel"];
		$trusted_commenttext = nl2br(trim(htmlspecialchars($comment_list[$i]["text"])));
		$trusted_avatarblock = "";
		$trusted_userstatus = "";
		if($trusted_acclevel > 2)
			$trusted_userstatus = "moderator";
		elseif($trusted_acclevel == 0)
			$trusted_userstatus = "banned";
		if($comment_list[$i]["avatar"] !== "")
			$trusted_avatarblock = '<img src="res/avatar/' . htmlspecialchars($comment_list[$i]["avatar"]) . '">';
		$trusted_editblock = "";
		if(strtotime($comment_list[$i]["editted"]) != -62169966000)
			$trusted_editblock = '<div class="text-edit">Last editted: ' . formattime($comment_list[$i]["editted"]) . '</div>';
		array_push($return_array, array("id" => $trusted_cid, "uid" => $trusted_uid, "username" => $trusted_username, 
				  "createdate" => $trusted_createdate, "acclevel" => $trusted_acclevel, "userstatus" => $trusted_userstatus, 
				  "text" => $trusted_commenttext, "avatarblock" => $trusted_avatarblock, "editblock" => $trusted_editblock));
	}
	return $return_array;
}
function create_commentpages($resultstotal)
{
	$perpage = get_comments_perpage();
	$pages = ceil($resultstotal / $perpage);
	if($pages == 1)
		return "";
	$curpage = 1;
	if(isset($_GET['commentpage']) && $_GET['commentpage'] > 0)
		$curpage = (int) $_GET['commentpage'];
	$pagegets = "";
	foreach($_GET as $index => $data)
	{
		if(is_string($data) && $index !== "commentpage" && $index !== "action")
			$pagegets .= htmlspecialchars("&{$index}={$data}");
	}
	$pages_division = "<div>";
	if($curpage - 2 > 1)
		$pages_division .= '<a href="bounty.php?commentpage=1' . $pagegets . '">first</a>&nbsp;';
	if($curpage - 2 >= 1)
		$pages_division .= '<a href="bounty.php?commentpage=' . ($curpage - 2) . $pagegets . '">' . ($curpage - 2) . '</a>&nbsp;';
	if($curpage - 1 >= 1)
		$pages_division .= '<a href="bounty.php?commentpage=' . ($curpage - 1) . $pagegets . '">' . ($curpage - 1) . '</a>&nbsp;';

	$pages_division .= $curpage . "&nbsp;";

	if($curpage + 1 <= $pages)
		$pages_division .= '<a href="bounty.php?commentpage=' . ($curpage + 1) . $pagegets . '">' . ($curpage + 1) .'</a>&nbsp;';
	if($curpage + 2 <= $pages)
		$pages_division .= '<a href="bounty.php?commentpage=' . ($curpage + 2) . $pagegets . '">' . ($curpage + 2) . '</a>&nbsp;';
	if($curpage + 2 < $pages)
		$pages_division .= '<a href="bounty.php?commentpage=' . $pages . $pagegets . '">last</a>';
	$pages_division .= "</div>";
	return $pages_division;
}

function display_comments($trusted_commentlist)
{
	$root = $_SERVER["DOCUMENT_ROOT"];
	if($trusted_commentlist[0] === "0")
	{
		echo '<div class="no-result">No comments</div>';
		return;
	}
	$perpage = get_comments_perpage();
	$count = count($trusted_commentlist) - 1;
	$pages_division = create_commentpages($trusted_commentlist[0]);
	echo $pages_division;
	for($i = 1; $i <= $count; $i++)
	{
		$current_comment = $trusted_commentlist[$i];
		include $root . '/include/template/t_comments_commentblock.php';
	}
	echo $pages_division;
}

function post_comment($db_handle, $user_array)
{
	if(!isset($_GET['id']) || !is_numeric($_GET['id']) || $_GET['id'] < 1)
	{
		header("Location: browse.php");
		exit;
	}
	$error_msg = "";
	if(!isset($_POST['submit']) || $_POST['submit'] !== "Send" || !isset($_POST['comment-data']))
		$error_msg = '<p id="error-msg">Couldn\'t send comment. Please try again.</p>';
	elseif(trim($_POST['comment-data']) === "")
		$error_msg = '<p id="error-msg">Your comment seems to be empty.</p>';
	elseif($user_array['uid'] === "0")
		$error_msg = '<p id="error-msg">You must login to comment.</p>';
	else
	{
		if(db_sendcomment($db_handle, $_GET['id'], $user_array['uid'], $_POST['comment-data']) === 0)
			$error_msg = '<p id="error-msg">Couldn\'t send comment. Please try again.</p>';
		header("Location: bounty.php?id=" . (int)$_GET['id']);
	}
	require_once 'viewbounty.php';
	viewbounty($db_handle, $user_array, $error_msg);
}

?>
