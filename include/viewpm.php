<?php
//
// viewpm.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
function get_trustedpm($db_handle, $pid, $user_array)
{
	$pm_array = db_getpm_bypid($db_handle, $pid);
	if($pm_array['pid'] !== "0" && $pm_array['receiver_uid'] === $user_array['uid'])
	{
		$trusted_pm = array();
		$trusted_pm['pid'] = (int)$pm_array['pid'];
		$trusted_pm['sender_uid'] = (int)$pm_array['sender_uid'];
		$trusted_pm['sender_username'] = htmlspecialchars($pm_array['sender_username']);
		$trusted_pm['receiver_uid'] = (int)$pm_array['receiver_uid'];
		$trusted_pm['createdate'] = formattime($pm_array['createdate']);
		$trusted_pm['read'] = (int)$pm_array['read'];
		$trusted_pm['subject'] = trim(htmlspecialchars($pm_array['subject']));
		$trusted_pm['text'] = nl2br(trim(htmlspecialchars($pm_array['text'])));

		$trusted_pm['sender_status'] = "";
		$trusted_pm['avatarblock'] = "";
		if($pm_array['sender_acclevel'] > 2)
			$trusted_pm['sender_status'] = "moderator";
		elseif($pm_array['sender_acclevel'] === "0")
			$trusted_pm['sender_status'] = "banned";
		if($pm_array['sender_avatar'] !== "")
			$trusted_pm['avatarblock'] = '<img src="res/avatar/' . htmlspecialchars($pm_array['sender_avatar']) . '">';
		if($trusted_pm['sender_uid'] === 0)
			$trusted_pm['usernameblock'] = "system";
		else
			$trusted_pm['usernameblock'] = "<a href=\"profile.php?id={$trusted_pm['sender_uid']}\">{$trusted_pm['sender_username']}</a>";
		db_pm_markasstatus($db_handle, $user_array['uid'], "read", array($trusted_pm['pid']));
		return $trusted_pm;
	}
	return array("pid" => "0");
}

function display_pm_error()
{
	$root = $_SERVER['DOCUMENT_ROOT'];
	$error['header'] = "View PM";
	$error['errormsg'] = "Could not retrieve PM. Maybe PM doesn't exist or it isn't yours.";
	$error['url'] = "inbox.php";
	$error['urltitle'] = "Return to inbox";
	include $root . "include/template/t_common_error.php";
}
function display_pm($trusted_pm)
{
	$root = $_SERVER['DOCUMENT_ROOT'];
	include $root . "include/template/t_viewpm_all.php";
}

function viewpm($db_handle, $user_array)
{
	$trusted_pm = get_trustedpm($db_handle, $_GET['id'], $user_array);
	display_header($user_array, "inbox");
	if($trusted_pm['pid'] !== "0")
	{
		display_pm($trusted_pm);
	}
	else
	{
		display_pm_error();
	}
	display_footer($user_array);
}
?>
