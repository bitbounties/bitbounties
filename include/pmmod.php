<?php
//
// pmmod.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//

function pm_markasread($db_handle, $user_array)
{
	if(!empty($_POST['msg']) && is_array($_POST['msg']))
		db_pm_markasstatus($db_handle, $user_array['uid'], "read", $_POST['msg']);

	header("Location: " . $_SERVER['REQUEST_URI']);
	exit;
}

function pm_markasunread($db_handle, $user_array)
{
	if(!empty($_POST['msg']) && is_array($_POST['msg']))
		db_pm_markasstatus($db_handle, $user_array['uid'], "unread", $_POST['msg']);
	header("Location: " . $_SERVER['REQUEST_URI']);
	exit;
}

function pm_delete($db_handle, $user_array)
{
	if(!empty($_POST['confirm']) && $_POST['confirm'] === "Yes")
	{
		if(!empty($_POST['msg']) && is_array($_POST['msg']))
			db_pm_delete($db_handle, $user_array['uid'], 0, $_POST['msg']);
		header("Location: " . $_SERVER['REQUEST_URI']);
		exit;
	}
	elseif(!empty($_POST['msg']) && empty($_POST['confirm']) && $_POST['action'] === "Delete")
	{
		pm_display_confirmation($db_handle, $user_array, $_POST['msg']);
	}
	else
	{
		header("Location: " . $_SERVER['REQUEST_URI']);
		exit;
	}
}

function pm_markallasread($db_handle, $user_array)
{
	db_pm_markasstatus($db_handle, $user_array['uid'], "allread");
	if($_SERVER['REQUEST_METHOD'] !== "GET")
	{
		header("Location: " . $_SERVER['REQUEST_URI']);
		exit;
	}
}

function pm_deleteall($db_handle, $user_array)
{
	if(!empty($_POST['confirm']) && $_POST['confirm'] === "Yes")
	{
		db_pm_delete($db_handle, $user_array['uid'], 1);
		header("Location: inbox.php");
		exit;
	}
	elseif(empty($_POST['confirm']) && $_POST['action'] === "Delete everything")
	{
		pm_display_confirmation($db_handle, $user_array);
	}
	else
	{
		header("Location: " . $_SERVER['REQUEST_URI']);
		exit;
	}
}

function pm_display_confirmation($db_handle, $user_array, $pmid_array = null)
{
	display_header($user_array, "inbox");
	echo '<div class="content"><h1>Delete PM</h1>';
	echo '<form action="inbox.php" method="post">';
	echo '<input type="hidden" name="action" value="' . htmlspecialchars($_POST['action']) . '">';
	if(!empty($pmid_array) || is_array($pmid_array))
	{
		$count = count($pmid_array);
		for($i = 0; $i < $count; $i++)
			echo '<input type="hidden" name="msg[]" value="' . htmlspecialchars($pmid_array[$i]) . '">';
		echo '<div class="common-box"><p>Warning: You are about to delete ' . $count . ' message(s).<br>Are you sure you want to continue?</p>';
	}
	else
	{
		echo '<div class="common-box"><p>Warning: You are about to delete all of your messages.<br>Are you sure you want to continue?</p>';
	}
	echo '<input type="submit" name="confirm" value="Yes"> <input type="submit" name="confirm" value="No">';
	echo '</div></form></div>';
	display_footer($user_array);
}
?>
