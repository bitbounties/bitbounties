<?php
//
// viewbounty.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
require_once 'comment.php';

function get_trustedbounty($db_handle, $request_id, $user_array)
{
	$bounty_array = db_getbounty_byid($db_handle, $request_id);
	if($bounty_array["id"] !== "0")
	{
		$category_array = db_getcategory_byid($db_handle, $bounty_array["category"]);
		if($category_array["id"] !== "0")
			$trusted_category = htmlspecialchars($category_array["name"]);
		else
			$trusted_category = "unknown";

		$trusted_id = (int)$bounty_array["id"];
		$trusted_uid = (int)$bounty_array["creator"];
		$trusted_username = db_getusername_byuid($db_handle, $trusted_uid);
		$trusted_title = htmlspecialchars($bounty_array["title"]);
		$trusted_desc = htmlspecialchars($bounty_array["description"]);
		$trusted_desc = nl2br($trusted_desc, false);
		$trusted_proof = htmlspecialchars($bounty_array["proof"]);
		$trusted_proof = nl2br($trusted_proof);
		$trusted_satoshi = formatbtc($bounty_array["satoshi"]);
		$trusted_litoshi = formatltc($bounty_array["litoshi"]);
		$trusted_createdate = formattime($bounty_array["createdate"]);
		$trusted_votes = (int)$bounty_array["votes"];
		$trusted_status = htmlspecialchars($bounty_array["status"]);
		$trusted_visible = (int)$bounty_array["visible"];
		$trusted_notes = "";
		if($bounty_array["autoclose"] === "1")
			$trusted_notes .= "Bounty closes by: " . formattime($bounty_array["autoclose_date"]) . "<br>";
		if(strtotime($bounty_array["edit_date"]) !== -62169966000)
			$trusted_notes .= "Last edited: ". formattime($bounty_array["edit_date"]) . "<br>";
		if($bounty_array["private"] === "1")
			$trusted_notes .= "This is a private bounty.<br>";
		if($trusted_visible == 0)
			$trusted_notes .= "This bounty is not visible";
		if($trusted_notes === "")
			$trusted_notes = "none";
		//check to see if we should show the bounty at all
		if($trusted_visible === 0 && ($user_array["uid"] === "0" || $user_array["acclevel"] < 3))
			return array("id" => "0");
		
		return array("id" => $trusted_id, "uid" => $trusted_uid, "username" => $trusted_username, "category" => $trusted_category,
					"title" => $trusted_title, "desc" => $trusted_desc, "proof" => $trusted_proof,
					"btc" => $trusted_satoshi, "ltc" => $trusted_litoshi, "createdate" => $trusted_createdate,
					"votes" => $trusted_votes, "status" => $trusted_status, "notes" => $trusted_notes);
	}
	return array("id" => "0");
}

function display_bounty_section($trusted_bounty)
{
	$root = $_SERVER["DOCUMENT_ROOT"];
	include $root . "/include/template/t_viewbounty_bountyblock.php";

}

function display_bounty_newcommentblock($trusted_commentform)
{
	$root = $_SERVER["DOCUMENT_ROOT"];
	include $root . "/include/template/t_viewbounty_newcommentblock.php";
}

function viewbounty($db_handle, $user_array, $trusted_comment_msg = "")
{
	if(!isset($_GET["id"]) || !is_numeric(($_GET["id"])))
	{
		header("Location: browse.php");
		exit;
	}
	$trusted_bounty = get_trustedbounty($db_handle, $_GET["id"], $user_array);
	if ($trusted_bounty["id"] !== "0")
		$trusted_commentlist = get_trustedcommentlist($db_handle, $trusted_bounty["id"], $user_array);
	
	display_header($user_array, "bounty");
	echo '<div class="content">';
	if($trusted_bounty["id"] === "0")
		echo '<p id="error-msg">Error: Specified bounty does not exist.</p>';
	else
	{
		$trusted_commentform = array("id" => $trusted_bounty['id'], "msg" => $trusted_comment_msg, "text" => "");
		if(isset($_POST['comment-text']))
			$trusted_commentform['text'] = htmlspecialchars($_POST['comment-text']);
		display_bounty_section($trusted_bounty);
		echo '<h2>Comments</h2>';
		display_comments($trusted_commentlist);
		if($user_array['uid'] > 0)
			display_bounty_newcommentblock($trusted_commentform);
	}
	echo '</div>'; //class=content
	display_footer($user_array);
}
?>
