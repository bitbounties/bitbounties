<?php
//
// viewinbox.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//

function get_pmlist($db_handle, $uid)
{
	if(!isset($_GET["page"]) || !is_numeric($_GET["page"]) || $_GET["page"] < 0)
		$cur_page = 1;
	else
		$cur_page = (int)$_GET["page"];
	$perpage = get_pms_perpage();
	return db_getpmlist_byreceiver($db_handle, $uid, ($cur_page - 1) * $perpage, $perpage);
}

function display_trustedpmrows($pmlist_array)
{
	$root = $_SERVER['DOCUMENT_ROOT'];
	$return_html = "";
	$total_results = $pmlist_array[0];
	$perpage = get_pms_perpage();
	$count = count($pmlist_array) - 1;
	for($i = 1; $i <= $count; $i++)
	{
		$trusted_row = array();
		$trusted_row['pid'] = (int)$pmlist_array[$i]['pid'];
		$trusted_row['sender_uid'] = (int)$pmlist_array[$i]['sender_uid'];
		$trusted_row['sender_username'] = htmlspecialchars($pmlist_array[$i]['sender_username']);
		$trusted_row['read'] = (int)$pmlist_array[$i]['read'];
		$trusted_row['createdate'] = formattime($pmlist_array[$i]['createdate']);
		$trusted_row['subject'] = trim(htmlspecialchars($pmlist_array[$i]['subject']));
		$trusted_row['read'] = $pmlist_array[$i]['read'] ? '' : 'class="bold"';
		if($trusted_row['sender_uid'] === 0)
			$trusted_row['usernameblock'] = "system";
		else
			$trusted_row['usernameblock'] = "<a href=\"profile.php?id={$trusted_row['sender_uid']}\">{$trusted_row['sender_username']}</a>";

		include $root . "/include/template/t_viewinbox_pmrow.php";
	}
}

function create_pmpages($resultstotal)
{
	$perpage = get_pms_perpage();
	$pages = ceil($resultstotal / $perpage);
	if($pages == 1)
		return "";
	$curpage = 1;
	if(isset($_GET['page']) && $_GET['page'] > 0)
		$curpage = (int) $_GET['page'];
	$pagegets = "";
	foreach($_GET as $index => $data)
	{
		if(is_string($data) && $index !== "page" && $index !== "action")
			$pagegets .= htmlspecialchars("&{$index}={$data}");
	}
	$pages_division = "<div>";
	if($curpage - 2 > 1)
		$pages_division .= '<a href="inbox.php?page=1' . $pagegets . '">first</a>&nbsp;';
	if($curpage - 2 >= 1)
		$pages_division .= '<a href="inbox.php?page=' . ($curpage - 2) . $pagegets . '">' . ($curpage - 2) . '</a>&nbsp;';
	if($curpage - 1 >= 1)
		$pages_division .= '<a href="inbox.php?page=' . ($curpage - 1) . $pagegets . '">' . ($curpage - 1) . '</a>&nbsp;';

	$pages_division .= $curpage . "&nbsp;";

	if($curpage + 1 <= $pages)
		$pages_division .= '<a href="inbox.php?page=' . ($curpage + 1) . $pagegets . '">' . ($curpage + 1) .'</a>&nbsp;';
	if($curpage + 2 <= $pages)
		$pages_division .= '<a href="inbox.php?page=' . ($curpage + 2) . $pagegets . '">' . ($curpage + 2) . '</a>&nbsp;';
	if($curpage + 2 < $pages)
		$pages_division .= '<a href="inbox.php?page=' . $pages . $pagegets . '">last</a>';
	$pages_division .= "</div>";
	return $pages_division;
}

function display_inbox_error()
{
	$root = $_SERVER["DOCUMENT_ROOT"];
	include $root . 'include/template/t_viewinbox_error.php';
}

function display_inbox($pmlist_array, $trusted_pagedivision)
{
	$root = $_SERVER["DOCUMENT_ROOT"];
	include $root . "include/template/t_viewinbox_all.php";
}

function viewinbox($db_handle, $user_array)
{
	$pmlist_array = get_pmlist($db_handle, (int)$user_array['uid']);
	display_header($user_array, "inbox");
	$results_total = $pmlist_array[0];
	if($results_total !== "0")
	{
		$pages_division = create_pmpages($results_total);
		display_inbox($pmlist_array, $pages_division);
	}
	else
	{
		display_inbox_error();
	}
	display_footer($user_array);
}
?>
