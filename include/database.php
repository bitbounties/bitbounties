<?php
//
// database.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
require_once 'bb_config.php';
require_once 'db/db_users.php';
require_once 'db/db_bounties.php';
require_once 'db/db_comments.php';
require_once 'db/db_pm.php';
function db_connect()
{
	$db_handle = mysqli_connect("localhost", BB_DB_USER, BB_DB_PASS, BB_DB_NAME);
	if(mysqli_connect_errno())
		fatalerror(1);
	mysqli_set_charset($db_handle, "UTF-8");
	return $db_handle;
}

function db_cleanup($db_handle)
{
	mysqli_close($db_handle);
}

function db_getcategory_byid ($db_handle, $untrusted_cid)
{
	if(!is_string($untrusted_cid))
		return array("id" => "0");
	$trusted_cid = mysqli_escape_string($db_handle, $untrusted_cid);
	$query = "SELECT * FROM category WHERE cid='{$trusted_cid}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(11);
	$row = mysqli_fetch_row($result);
	if(!$row)
		return array("id" => "0");
	return array("id" => $row[0], "name" => $row[1], "count" => $row[2]);
}

?>
