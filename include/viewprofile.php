<?php
//
// profile.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//

function get_profile_array($db_handle, $profileid)
{
	$profile_array = db_getuser_byuid_fullassoc($db_handle, $_GET['id']);
	if($profile_array['uid'] === "0")
		return $profile_array;
	$trusted_profile = array();
	$trusted_profile['uid'] = (int)$profile_array['uid'];
	$trusted_profile['username'] = htmlspecialchars($profile_array['username']);
	$trusted_profile['lastseen'] = formattime($profile_array['lastlogin']);
	$trusted_profile['joindate'] = date("M Y", strtotime($profile_array['createdate']));
	$trusted_profile['options'] = (int)$profile_array['options'];
	$trusted_profile['noteblock'] = '';
	$trusted_profile['avatarblock'] = '';

	switch($profile_array['acclevel'])
	{
		case "0":
			$trusted_profile['level'] = "banned";
			break;
		case "1":
			$trusted_profile['level'] = "user";
			break;
		case "2":
			$trusted_profile['level'] = "escrow";
			break;
		case "3":
			$trusted_profile['level'] = "moderator";
			break;
		case "4":
			$trusted_profile['level'] = "sysop";
			break;
		default:
			$trusted_profile['level'] = "unknown";
	}
	if($profile_array['name'] !== '')
		$trusted_profile['name'] = htmlspecialchars($profile_array['name']);
	if($profile_array['age'] !== '')
		$trusted_profile['age'] = (int) $profile_array['age'];
	if($profile_array['gender'] !== 'Not set')
		$trusted_profile['gender'] = htmlspecialchars($profile_array['gender']);
	if($profile_array['location'] !== '')
		$trusted_profile['location'] = htmlspecialchars($profile_array['location']);

	if($profile_array['notes'] !== '')
	{
		$trusted_profile['noteblock'] = '<div class="bounty-section"><div class="alttable-header">Notes</div>' .
			'<div class="bounty-desc">' . nl2br(htmlspecialchars(trim($profile_array['notes']))) . '</div></div>';
	}

	$count = 0;
	$trusted_profile['optblock'] = '<tr>';
	if(!empty($trusted_profile['name']))
	{
		$trusted_profile['optblock'] .= '<td class="alttable-header">Name</td><td class="alttable-data">' .
			$trusted_profile['name'] . '</td>';
		$count++;
	}
	if(!empty($trusted_profile['location']))
	{
		$trusted_profile['optblock'] .= '<td class="alttable-header">Location</td><td class="alttable-data">' .
			$trusted_profile['location'] . '</td>';
		$count++;
	}
	if(!empty($trusted_profile['age']))
	{
		if($count == 2)
			$trusted_profile['optblock'] .= '</tr><tr>';
		$trusted_profile['optblock'] .= '<td class="alttable-header">Age</td><td class="alttable-data">' .
			$trusted_profile['age'] . '</td>';
		$count++;
	}
	if(!empty($trusted_profile['gender']))
	{
		if($count == 2)
			$trusted_profile['optblock'] .= '</tr><tr>';
		$trusted_profile['optblock'] .= '<td class="alttable-header">Gender</td><td class="alttable-data">' .
			$trusted_profile['gender'] . '</td>';
		$count++;
	}
	if($count != 0)
	{
		$trusted_profile['optblock'] .= '</tr>';
		$trusted_profile['optblock'] = lastrow_replace("alttable-header", "alttable-header bl", $trusted_profile['optblock']);
		$trusted_profile['optblock'] = lastrow_rreplace("alttable-data", "alttable-data br", $trusted_profile['optblock']);
	}
	else
		$trusted_profile['optblock'] = '';
	if($profile_array['avatar'] !== '')
	{
		$trusted_profile['avatarblock'] = '<td class="alttable-avatar" rowspan="'. ((ceil($count / 2)) + 2 ) . '">' .
			'<img src="res/avatar/' . rawurlencode($profile_array['avatar']) . '"></td>';
	}
	return $trusted_profile;
}

function display_profile($trusted_profile)
{
	$root = $_SERVER["DOCUMENT_ROOT"];
	include $root . "/include/template/t_viewprofile_all.php";
}

function display_profile_error()
{
	$root = $_SERVER['DOCUMENT_ROOT'];
	$error['header'] = "View Profile";
	$error['errormsg'] = "Specified profile does not exist.";
	$error['url'] = "profile.php";
	$error['urltitle'] = "Return to your profile";
	include $root . "include/template/t_common_error.php";
}

function viewprofile($db_handle, $user_array)
{
	if(empty($_GET['id']) || !is_numeric($_GET['id']))
	{
		header("Location: profile.php");
		exit;
	}
	$profile_id = (int) $_GET['id'];
	$trusted_profile = get_profile_array($db_handle, $profile_id);
	display_header($user_array, "profile");
	if($trusted_profile['uid'] === "0")
		display_profile_error();
	else
		display_profile($trusted_profile);
	display_footer($user_array);
}
?>
