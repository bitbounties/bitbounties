<?php 
//
// footer.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//

function display_footer($user_array)
{
	$output = '<footer>© 2016 Bitbounties.org';
	if(isset($user_array["acclevel"]) && $user_array["acclevel"] >= 3)
	{
		$output .= "<br>you are logged in as a moderator.";
		$output .= sprintf(" Page generated in %.2fms", (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']) * 1000);
	}
	echo $output;
}
?>
