<div class="content">
	<h1 style='text-align: center;'>Inbox</h1>
	[<a href="inbox.php?action=new">Send a PM</a>]
	<div class="really-thin">
		<?php echo $trusted_pagedivision; ?>
		<table class="bounty-section">
			<form action="inbox.php" id="messageform" method="post">
				<tr>
					<td class="table-header tl" colspan="2">Title</td>
					<td class="table-header">Sender</td>
					<td class="table-header tr">Date</td>
				</tr>
				<?php display_trustedpmrows($pmlist_array); ?>
				<tr>
					<td class="table-header bl"><input type="checkbox" onclick="toggleChecks(this)"></td>
					<td class="table-header left">
						<input type="submit" name="action" value="Mark as read">
						<input type="submit" name="action" value="Mark as unread">
						<input type="submit" name="action" value="Delete">
					</td>
					<td class="table-header br" colspan="2">
						<input type="submit" name="action" value="Mark all as read">
						<input type="submit" name="action" value="Delete everything">
					</td>
				</tr>
			</form>
		</table>
		<?php echo $trusted_pagedivision; ?>
	</div>
</div>
<script>
function toggleChecks(source)
{
  checkboxes = document.getElementsByName('msg[]=');
  for(var item of checkboxes)
    item.checked = source.checked;
}
</script>
