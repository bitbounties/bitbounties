<h1>View Bounty</h1>
<div>
	[<a href="report.php?id=<?php echo $trusted_bounty['id']; ?>">report</a>] -
	[<a href="claim.php?id=<?php echo $trusted_bounty['id']; ?>">claim bounty</a>] -
	[<a href="bounty.php?action=vote&amp;id=<?php echo $trusted_bounty['id']; ?>">vote</a>]
</div>
<div class="thin">
	<table class="bounty-section">
		<tr>
			<td class="alttable-header tl">Title</td>
			<td class="alttable-data"><?php echo $trusted_bounty['title']; ?></td>
			<td class="alttable-header">bounty</td>
			<td class="alttable-data tr"><?php echo $trusted_bounty['btc'] . $trusted_bounty['ltc']; ?></td>
		</tr>
		<tr>
			<td class="alttable-header">Created</td>
			<td class="alttable-data"><?php echo $trusted_bounty['createdate']; ?>
				by <a href="profile.php?id=<?php echo $trusted_bounty['uid']; ?>"><?php echo $trusted_bounty['username']; ?></a></td>
			<td class="alttable-header">Category</td>
			<td class="alttable-data"><?php echo $trusted_bounty['category']; ?></td>
		</tr>
		<tr>
			<td class="alttable-header">Votes</td>
			<td class="alttable-data"><?php echo $trusted_bounty['votes']; ?></td>
			<td class="alttable-header">Status</td>
			<td class="alttable-data"><?php echo $trusted_bounty['status']; ?></td>
		</tr>
		<tr>
			<td class="alttable-header bl">Notes</td>
			<td class="alttable-data br" colspan="3"><?php echo $trusted_bounty['notes']; ?></td>
		</tr>
	</table>
	<div class="bounty-section">
		<div class="alttable-header">Description</div>
		<div class="bounty-desc">
			<?php echo $trusted_bounty['desc']; ?>
		</div>
	</div>
	<div class="bounty-section">
		<div class="alttable-header">Verification</div>
		<div class="bounty-desc">
			<?php echo $trusted_bounty['proof']; ?>
		</div>
	</div>
	<div>
		[<a href="report.php?id=<?php echo $trusted_bounty['id']; ?>">report</a>] -
		[<a href="claim.php?id=<?php echo $trusted_bounty['id']; ?>">claim bounty</a>] -
		[<a href="bounty.php?action=vote&amp;id=<?php echo $trusted_bounty['id']; ?>">vote</a>]
	</div>
</div>
