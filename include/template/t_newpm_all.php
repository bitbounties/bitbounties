<div class="content">
	<h1 style='text-align: center;'>Inbox</h1>
	{trusted_pm_msg}
	<div class="common-box">
		<form action="inbox.php?action=new" method="post">
			<input type="hidden" name="mode" value="{trusted_pm_mode}">
			<table class="form-table right">
			<tr class="left">
				<td><label for="recipient" class="right">Recipient</label></td>
				<td><input type="textbox" id="recipient" name="recipient" class="common-textbox" value="{trusted_pm_recipient}" required></td>
			</tr>
			<tr>
				<td><label for="subject" class="right">Subject</label></td>
				<td class="max-width">
					<input type="textbox" id="subject" name="subject" class="common-textbox max-width" value="{trusted_pm_subject}" required>
				</td>
				</tr>
			<tr>
				<td colspan="2">
					<textarea id="text" rows="24" cols="80" name="text" class="common-textbox max-width">{trusted_pm_text}</textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="center"><input type="submit" name="submit" value="Send"></td>
			</tr>
			</table>
		</form>
	</div>
</div>
