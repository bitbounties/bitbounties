<div class="content">
	<h1>View PM</h1>
	[<a href="inbox.php">Return to Inbox</a>]
	<div class="really-thin">
		<div class="comments left">
			<div class="comment-head">
				<?php echo $trusted_pm['createdate']; ?> <strong>"<?php echo $trusted_pm['subject']; ?>"</strong>
			</div>
			<table class="comment-body">
				<tr>
					<td class="comment-userbox">
						<?php echo $trusted_pm['usernameblock']; ?>
						<br><?php echo $trusted_pm['sender_status']; ?>
						<br><br><?php echo $trusted_pm['avatarblock']; ?>
					</td>
					<td class="comment-text">
						<?php echo $trusted_pm['text']; ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<h2>Reply</h2>
	<div class="common-box">
		<form action="inbox.php?action=new" method="post">
			<input type="hidden" name="recipient" class="common-textbox" value="<?php echo $trusted_pm['sender_uid']; ?>">
			<input type="hidden" name="mode" class="common-textbox" value="uid">
			<table class="form-table right">
			<tr>
				<td><label for="subject" class="right">Subject</label></td>
				<td class="max-width">
					<input type="textbox" id="subject" name="subject" class="common-textbox max-width" value="Re: <?php echo $trusted_pm['subject']; ?>" required>
				</td>
				</tr>
			<tr>
				<td colspan="2">
					<textarea id="text" rows="24" cols="80" name="text" class="common-textbox max-width"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="center"><input type="submit" name="submit" value="Send"></td>
			</tr>
			</table>
		</form>
	</div>
</div>
