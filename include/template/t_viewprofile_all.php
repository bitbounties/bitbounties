<?php
$lr_br = "";
$lr_bl = "";
if(empty($trusted_profile['optblock']))
{
	//status & lastseen headers are the last row
	$lr_br = " br";
	$lr_bl = " bl";

}
elseif(!empty($trusted_profile['optblock']))
{

}
?>
<div class="content">
	<h1>View Profile</h1>
	<div>
		[<a href="report.php?uid=<?php echo $trusted_profile['uid']; ?>">report</a>] -
		[<a href="inbox.php?action=new&id=<?php echo $trusted_profile['uid']; ?>">send message</a>]
	</div>
	<div class="thin">

		<table class="bounty-section">

			<tr>
				<?php echo $trusted_profile['avatarblock']; ?>
				<td class="alttable-header tl">Username</td>
				<td class="alttable-data left"><?php echo $trusted_profile['username']; ?></td>
				<td class="alttable-header">Join date</td>
				<td class="alttable-data left tr"><?php echo $trusted_profile['joindate']; ?></td>
			</tr>
			<tr>
				<td class="alttable-header<?php echo $lr_bl; ?>">Status</td>
				<td class="alttable-data left"><?php echo $trusted_profile['level']; ?></td>
				<td class="alttable-header">Last seen</td>
				<td class="alttable-data left<?php echo $lr_br; ?>"><?php echo $trusted_profile['lastseen']; ?></td>
			</tr>
			<?php echo $trusted_profile['optblock']; ?>

		</table>
		<?php echo $trusted_profile['noteblock']; ?>
	</div>
</div>
