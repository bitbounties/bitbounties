<div class="content">
	<h1 style='text-align: center;'>Browse Bounties</h1>
	<div class="thin">
		<?php require docroot() . 'include/template/t_browse_searchbox.php'; ?>
		<?php echo $trusted_browsepages_block; ?>
		<table class="bounty-section">
			<tr>
				<td class="table-header tl">Title</td>
				<td class="table-header">bounty</td>
				<td class="table-header">status</td>
				<td class="table-header">category</td>
				<td class="table-header">created</td>
				<td class="table-header">votes</td>
				<td class="table-header tr">comments</td>
			</tr>
			<?php
			$count = $trusted_resultrows[0];
			for($i = 1; $i <= $count; $i++)
			{
				$trusted_row = $trusted_resultrows[$i];
				require docroot() . 'include/template/t_browse_resultrow.php';
			}
			?>
		</table>
		<?php echo $trusted_browsepages_block; ?>
	</div>
</div>
