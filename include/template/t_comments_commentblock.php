<div class="comments left" id="c<?php echo $current_comment['id']; ?>">
	<div class="comment-head">
		<a href="#c<?php echo $current_comment['id']; ?>">#<?php echo $current_comment['id']; ?></a>
		<?php echo $current_comment['createdate']; ?>
	</div>
	<table class="comment-body">
		<tr>
			<td class="comment-userbox">
				<a href="profile.php?id=<?php echo $current_comment['uid']; ?>"><?php echo $current_comment['username']; ?></a>
				<br><?php echo $current_comment['userstatus']; ?>
				<br><br><?php echo $current_comment['avatarblock']; ?>
			</td>
			<td class="comment-text">
				<?php echo $current_comment['text']; ?>
				<?php echo $current_comment['editblock']; ?>
			</td>
		</tr>
	</table>
</div>
