<?php
function searchbox_checkbox($checkbox)
{
	if(!empty($_GET[$checkbox]) && is_string($_GET[$checkbox]) && $_GET[$checkbox])
		return " checked";
	return "";
}

function searchbox_query()
{
	if(!empty($_GET['search']) && is_string($_GET['search']))
		return trim(htmlspecialchars($_GET['search']));
	return "";
}
?>
<div class="searchbox">
	<form action="browse.php" method="get">
		<div>
			Category:
			<input type="checkbox" id="cat1" value="1" name="cat1"<?php echo searchbox_checkbox("cat1"); ?>>
				<label for="cat1">File Request</label>
			<input type="checkbox" id="cat2" value="1" name="cat2"<?php echo searchbox_checkbox("cat2"); ?>>
				<label for="cat2">Program/Service</label>
			<input type="checkbox" id="cat3" value="1" name="cat3"<?php echo searchbox_checkbox("cat3"); ?>>
				<label for="cat3">Contest</label>
			<input type="checkbox" id="cat4" value="1" name="cat4"<?php echo searchbox_checkbox("cat4"); ?>>
				<label for="cat4">Misc</label>
		</div>
		<div>
			Status:
			<input type="checkbox" id="open" value="1" name="open"<?php echo searchbox_checkbox("open"); ?>>
				<label for="open">Open</label>
			<input type="checkbox" id="closed" value="1" name="closed"<?php echo searchbox_checkbox("closed"); ?>>
				<label for="closed">Closed</label>
			<input type="checkbox" id="filled" value="1" name="filled"<?php echo searchbox_checkbox("filled"); ?>>
				<label for="filled">Filled</label>
			<input type="checkbox" id="pending" value="1" name="pending"<?php echo searchbox_checkbox("pending"); ?>>
				<label for="pending">Pending approval</label>
		</div>
		<input type="text" class="common-textbox" name="search" value="<?php echo searchbox_query(); ?>">
		<input type="submit" value="Filter">
	</form>
</div>
