<?php
//
// db_users.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
function db_getuser_byname($db_handle, $untrusted_username)
{
	if(!is_string($untrusted_username))
		return array(0 => "0");
	$trusted_user = mysqli_escape_string($db_handle, $untrusted_username);
	$query = "SELECT uid,username,password,acclevel,satoshi,litoshi,alerts FROM users WHERE username='{$trusted_user}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(2);
	$user_iuplsla = mysqli_fetch_row($result);
	mysqli_free_result($result);
	if(!$user_iuplsla)
		return array(0 => "0");
	return $user_iuplsla;
}

function db_getuser_byuid($db_handle, $untrusted_uid)
{
	if(!is_numeric($untrusted_uid))
		return array(0 => "0");
	$trusted_uid = (int)$untrusted_uid;
	$query = "SELECT uid, username, cookieval, acclevel, satoshi, litoshi, alerts FROM users WHERE uid='{$trusted_uid}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(6);
	$user_iuclsla = mysqli_fetch_row($result);
	mysqli_free_result($result);
	if(!$user_iuclsla)
		return array(0 => "0");
	return $user_iuclsla;
}

function db_getusername_byuid($db_handle, $untrusted_uid)
{
	$user_iuclsla = db_getuser_byuid($db_handle, $untrusted_uid);
	if($user_iuclsla[0] === "0")
		return "unknown";
	return $user_iuclsla[1];
}

function db_setuser_cookieval($db_handle, $untrusted_uid, $cookieval)
{
	if(!is_numeric($untrusted_uid))
		return;
	$trusted_uid = (int)$untrusted_uid;
	$query = "UPDATE users SET cookieval='{$cookieval}' WHERE uid ='{$trusted_uid}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(3);
	if(mysqli_affected_rows($db_handle) != 1)
		fatalerror(4);
}

function db_unsetuser_cookieval($db_handle, $untrusted_uid)
{
	if(!is_numeric($untrusted_uid))
		fatalerror(12);
	$trusted_uid = (int)$untrusted_uid;
	$query = "UPDATE users SET cookieval='' WHERE uid ='{$trusted_uid}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(7);
}

function db_getuser_byuid_assoc($db_handle, $id)
{
	$user_iuclsla = db_getuser_byuid($db_handle, $id);
	if($user_iuclsla[0] === "0")
		return array("uid" => "0");
	return array("uid" => $user_iuclsla[0], "username" => $user_iuclsla[1], "acclevel" => $user_iuclsla[3],
				"satoshi" => $user_iuclsla[4], "litoshi" => $user_iuclsla[5], "alerts" => $user_iuclsla[6]);
}

function db_getuser_byname_assoc($db_handle, $username)
{
	$user_iuplsla = db_getuser_byname($db_handle, $username);
	if($user_iuplsla[0] === "0")
		return array("uid" => "0");
	return array("uid" => $user_iuplsla[0], "username" => $user_iuplsla[1], "acclevel" => $user_iuplsla[3],
				"satoshi" => $user_iuplsla[4], "litoshi" => $user_iuplsla[5]);
}

function db_unsetuser_alerts($db_handle, $untrusted_uid)
{
	if(!is_numeric($untrusted_uid))
		fatalerror(18);
	$trusted_uid = (int)$untrusted_uid;
	$query = "UPDATE `users` SET `alerts`='0' WHERE uid ='{$trusted_uid}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(18);
}

function db_getuser_byuid_fullassoc($db_handle, $untrusted_uid)
{
	if(!is_numeric($untrusted_uid))
		return array("uid" => "0");
	$trusted_uid = (int)$untrusted_uid;
	$query = "SELECT uid, username, acclevel, satoshi, litoshi, avatar, lastlogin, createdate," .
	"opt_name, opt_age, opt_gender, opt_location, opt_notes, options " .
		"FROM users WHERE uid='{$trusted_uid}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(21);
	$user_array = mysqli_fetch_row($result);
	mysqli_free_result($result);
	if(!$user_array)
		return array("uid" => "0");
	return array("uid" => $user_array[0], "username" => $user_array[1], "acclevel" => $user_array[2],
				 "satoshi" => $user_array[3], "litoshi" => $user_array[4], "avatar" => $user_array[5],
				 "lastlogin" => $user_array[6], "createdate" => $user_array[7],
				 "name" => $user_array[8], "age" => $user_array[9], "gender" => $user_array[10],
				 "location" => $user_array[11], "notes" => $user_array[12], "options" => $user_array[13]);
}

function db_setuser_lastlogin($db_handle, $untrusted_uid, $untrusted_ip)
{
	if(!is_numeric($untrusted_uid) || !is_string($untrusted_ip) || strlen($untrusted_ip) > 45)
		return;
	$trusted_uid = (int)$untrusted_uid;
	$trusted_ip = mysqli_escape_string($db_handle, $untrusted_ip);
	$query = "UPDATE `users` SET `lastlogin`=CURRENT_TIMESTAMP, `last_ip`='{$trusted_ip}' WHERE uid ='{$trusted_uid}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(22);
}
?>
