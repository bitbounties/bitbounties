<?php
//
// db_bounties.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
function db_getbounty_byid($db_handle, $untrusted_id)
{
	if(!is_numeric($untrusted_id))
		return array("id" => "0");
	$trusted_id = (int)$untrusted_id;
	$query = "SELECT `id`, `creator`, `title`, `satoshi`, `litoshi`, `createdate`, `category`, `votes`, `comments`, `status`, `autoclose`, ";
	$query .= "`autoclose_date`, `private`, `visible`, `edit_date`, `description`, `proof` FROM bounties WHERE `id`='{$trusted_id}';";
	$result = mysqli_query($db_handle, $query);
	if(!$result)
		fatalerror(8);
	$row = mysqli_fetch_row($result);
	mysqli_free_result($result);
	if(!$row)
		return array("id" => "0");
	return array("id" => $row[0], "creator" => $row[1], "title" => $row[2], "satoshi" => $row[3], "litoshi" => $row[4],
			"createdate" => $row[5], "category" => $row[6], "votes" => $row[7], "comments" => $row[8], "status" => $row[9], "autoclose" => $row[10],
			"autoclose_date" => $row[11], "private" => $row[12], "visible" => $row[13], "edit_date" => $row[14],
			"description" => $row[15], "proof" => $row[16]);
}

function db_getbountylist($db_handle, $start, $count, $categories_array, $status_array, $visible, $untrusted_search_string, $sortby, $sortmode)
{
	if(is_numeric($count) && is_numeric($start) && is_string($untrusted_search_string) && is_string($sortby) && is_string($sortmode)
			&& is_array($categories_array) && is_array($status_array))
	{
		$trusted_query_cat_status = "";
		$trusted_query_sort = "";
		$trusted_query_search = "";
		$trusted_query_limit = "";
		//$trusted_query_visible
		//$trusted_query_sort
			
		$query_cat = "(";
		$query_status = "(";
		foreach($categories_array as $val)
		{
			if(!is_numeric($val))
				return array("0");
			$query_cat .= "`category`='{$val}' OR ";
		}
		if($query_cat !== "(")
		{
			$query_cat = substr($query_cat, 0, -3);
			$query_cat .= ")";
		}
		else
			$query_cat = "";
			
			
		foreach($status_array as $val)
		{
			switch($val)
			{
				case "open":
					$query_status .= "`status`='open' OR ";
					break;
				case "closed":
					$query_status .= "`status`='closed by creator' OR `status`='closed by autoclose' OR `status`='closed by moderator' OR ";
					break;
				case "filled":
					$query_status .= "`status`='claimed, paid' OR ";
					break;
				case "pending":
					$query_status .= "`status`='claimed, pending' OR ";
					break;
			}
		}
		if($query_status !== "(")
		{
			$query_status = substr($query_status, 0, -3);
			$query_status .= ")";
		}
		else
			$query_status = "";
		if($query_cat === "" && $query_status === "")
			$trusted_query_cat_status = "";
		else if($query_cat !== "" && $query_status !== "")
			$trusted_query_cat_status = "AND {$query_cat} AND {$query_status}";
		else
			$trusted_query_cat_status = "AND {$query_cat}{$query_status}";
			
			
		if($visible == 0)
			$trusted_query_visible = "`visible`='0'";
		else
			$trusted_query_visible = "`visible`='1'";
			
		if($untrusted_search_string !== "")
			$trusted_query_search = "AND `title` LIKE '%" . mysqli_escape_string($db_handle, $untrusted_search_string) . "%'";
			
		switch($sortby)
		{
			case "id":
				$trusted_query_sort = "ORDER BY `id`";
				break;
			case "bounty":
				$trusted_query_sort = "ORDER BY `satoshi` + (`litoshi` * '" . BB_LTCBTC . "')";
				break;
			case "vote":
				$trusted_query_sort = "ORDER BY `votes`";
				break;
			case "comments":
				$trusted_query_sort = "ORDER BY `comments`";
				break;
		}
		if($trusted_query_sort !== "")
		{
			if(strcasecmp($sortmode, "desc") == 0)
				$trusted_query_sort .= " DESC";
			else
				$trusted_query_sort .= "ASC";
		}
		else
			$trusted_query_sort = "ORDER BY `id` DESC";

		$trusted_query_limit = "LIMIT {$start},{$count}";
		$query = "SELECT `id`, `creator`, `title`, `satoshi`, `litoshi`, `createdate`, `category`, `votes`, `comments`, " .
				"`status`, `visible` FROM `bounties`  WHERE {$trusted_query_visible} {$trusted_query_search} " .
				"{$trusted_query_cat_status} {$trusted_query_sort} ${trusted_query_limit};";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(10);
		$return_array = array("0");
		while($row = mysqli_fetch_row($result))
			array_push($return_array, array("id" => $row[0], "creator" => $row[1], "title" => $row[2], "satoshi" => $row[3],
					"litoshi" => $row[4], "createdate" => $row[5], "category" => $row[6], "votes" => $row[7], "comments" => $row[8],
					"status" => $row[9], "visible" => $row[10]));
		mysqli_free_result($result);
		$query = "SELECT COUNT(id) FROM `bounties`  WHERE {$trusted_query_visible} {$trusted_query_search} {$trusted_query_cat_status};";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(10);
		$row = mysqli_fetch_row($result);
		if($row[0] - $start > 0)
			$return_array[0] = $row[0];

		mysqli_free_result($result);
		return $return_array;
	}
	return array("0");
}
?>
