<?php
//
// db_pm.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
function db_getpmlist_byreceiver($db_handle, $untrusted_uid, $start, $count)
{
	if(is_numeric($untrusted_uid) && is_numeric($start) && is_numeric($count))
	{
		$trusted_uid = (int) $untrusted_uid;
		$query = "SELECT `pm`.`pid`, `pm`.`sender_uid`, `users`.`username`, `pm`.`read`, " .
				"`pm`.`createdate`, `pm`.`subject` FROM `pm` JOIN `users` ON `pm`.`sender_uid` = `users`.`uid` " .
				"WHERE `pm`.`receiver_uid` ='{$trusted_uid}' ORDER BY `pid` DESC LIMIT {$start},{$count};";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(16);
		$return_array = array("0");
		while($row = mysqli_fetch_row($result))
			array_push($return_array, array("pid" => $row[0], "sender_uid" => $row[1], "sender_username" => $row[2],
					 "read" => $row[3], "createdate" => $row[4], "subject" => $row[5]));
		mysqli_free_result($result);
		$query = "SELECT COUNT(pid) FROM `pm` WHERE `pm`.`receiver_uid` = '{$trusted_uid}';";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(16);
		$row = mysqli_fetch_row($result);
		if($row[0] - $start > 0)
			$return_array[0] = $row[0];
		mysqli_free_result($result);
		return $return_array;
	}
	return array("0");
}

function db_getpm_bypid($db_handle, $untrusted_pid)
{
	if(is_numeric($untrusted_pid))
	{
		$trusted_pid = (int) $untrusted_pid;
		$query = "SELECT `pm`.`pid`, `pm`.`sender_uid`, `pm`.`receiver_uid`, `users`.`username`, `users`.`avatar`, " .
				"`users`.`acclevel`, `pm`.`read`, `pm`.`createdate`, `pm`.`subject`, `pm`.`text` " .
				"FROM `pm` JOIN `users` ON `pm`.`sender_uid` = `users`.`uid` " .
				"WHERE `pm`.`pid` ='{$trusted_pid}' LIMIT 1;";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(16);
		$row = mysqli_fetch_row($result);
		if(!$row)
			return array("pid" => "0");
		$return_array = array("pid" => $row[0], "sender_uid" => $row[1], "receiver_uid" => $row[2], "sender_username" => $row[3],
					"sender_avatar" => $row[4], "sender_acclevel" => $row[5], "read" => $row[6], "createdate" => $row[7],
					"subject" => $row[8], "text" => $row[9]);
		mysqli_free_result($result);
		return $return_array;
	}
	return array("pid" => "0");
}

function db_sendpm($db_handle, $untrusted_sender_uid, $untrusted_receiver_uid, $untrusted_sender_ip, $untrusted_subject, $untrusted_text)
{
	if(is_numeric($untrusted_sender_uid) && is_numeric($untrusted_receiver_uid) && is_string($untrusted_sender_ip)
	  && is_string($untrusted_subject) && is_string($untrusted_text) && !empty(trim($untrusted_subject))
	  && !empty(trim($untrusted_text)) && !empty($untrusted_receiver_uid))
	{
		$trusted_sender_uid = (int)$untrusted_sender_uid;
		$trusted_receiver_uid = (int)$untrusted_receiver_uid;
		$trusted_sender_ip = mysqli_escape_string($db_handle, $untrusted_sender_ip);
		$trusted_subject = mysqli_escape_string($db_handle, trim($untrusted_subject));
		$trusted_text = mysqli_escape_string($db_handle, trim($untrusted_text));

		$query = "INSERT INTO `pm` SET `sender_uid`='{$trusted_sender_uid}', `receiver_uid`='{$trusted_receiver_uid}', " .
				"`sender_ip`='{$trusted_sender_ip}', `subject`='{$trusted_subject}', `text`='{$trusted_text}';";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(17);
		$query = "UPDATE `users` SET `alerts`=`alerts`+1 WHERE `uid`='{$trusted_receiver_uid}'";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(17);
		return 1;
	}
	return 0;
}

function db_pm_markasstatus($db_handle, $untrusted_owneruid, $status, $untrusted_pmid = null)
{
	if(is_numeric($untrusted_owneruid) && is_string($status))
	{
		$trusted_owneruid = (int)$untrusted_owneruid;
		$trusted_pmid_query = "AND (";
		if($status === "read")
		{
			if(empty($untrusted_pmid) || !isset($untrusted_pmid[0]))
				return 1;
			$trusted_set_query = "SET `read`='1'";
			foreach($untrusted_pmid as $element)
				$trusted_pmid_query .= "`pid`='" . (int)$element . "' OR ";
			$trusted_pmid_query = substr($trusted_pmid_query, 0, -4);
			$trusted_pmid_query .= ')';
		}
		elseif($status === "unread")
		{
			if(empty($untrusted_pmid) || !isset($untrusted_pmid[0]))
				return 1;
			$trusted_set_query = "SET `read`='0'";
			foreach($untrusted_pmid as $element)
				$trusted_pmid_query .= "`pid`='" . (int)$element . "' OR ";
			$trusted_pmid_query = substr($trusted_pmid_query, 0, -4);
			$trusted_pmid_query .= ')';
		}
		elseif($status === "allread")
		{
			$trusted_set_query = "SET `read`='1'";
			$trusted_pmid_query = "";
		}
		elseif($status === "allunread")
		{
			$trusted_set_query = "SET `read`='0'";
			$trusted_pmid_query = "";
		}
		else
			return 1;

		$query = "UPDATE `pm` {$trusted_set_query} WHERE `receiver_uid`='{$trusted_owneruid}' {$trusted_pmid_query};";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(19);
		return 0;
	}
	return 1;
}

function db_pm_delete($db_handle, $untrusted_owneruid, $delall, $untrusted_pmid = null)
{
	if(is_numeric($untrusted_owneruid) && is_int($delall))
	{
		$trusted_owneruid = (int)$untrusted_owneruid;
		$trusted_pmid_query = "AND (";
		if($delall == 0)
		{
			if(empty($untrusted_pmid) || !isset($untrusted_pmid[0]))
				return 1;
			foreach($untrusted_pmid as $element)
				$trusted_pmid_query .= "`pid`='" . (int)$element . "' OR ";
			$trusted_pmid_query = substr($trusted_pmid_query, 0, -4);
			$trusted_pmid_query .= ')';
		}
		elseif($delall == 1)
		{
			$trusted_pmid_query = "";
		}
		else
			return 1;
		$query = "DELETE FROM `pm` WHERE `receiver_uid`='{$trusted_owneruid}' {$trusted_pmid_query};";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(20);
		return 0;
	}
	return 1;
}
?>
