<?php
//
// db_comments.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
function db_getcommentlist_bybounty($db_handle, $id, $start, $count, $show_invisible)
{
	if(is_numeric($id) && is_numeric($id) && is_numeric($start) && is_numeric($count) && is_numeric($show_invisible))
	{
		if($show_invisible === 1)
			$invisible = "AND `comments`.`visible` = '1'";
		else
			$invisible = "";
		$query = "SELECT `comments`.`id`, `users`.`uid`, `users`.`username`, `users`.`acclevel`, `users`.`avatar`, `comments`.`createdate`, `comments`.`editdate`, " .
				"`comments`.`visible`, `comments`.`text` FROM `comments` JOIN `users` on `comments`.`user` = `users`.`uid` " .
				"WHERE `comments`.`bid` = '{$id}' {$invisible} ORDER BY `id` DESC LIMIT {$start},{$count};";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(13);
		$return_array = array("0");
		while($row = mysqli_fetch_row($result))
			array_push($return_array, array("id" => $row[0], "uid" => $row[1], "username" => $row[2], "acclevel" => $row[3], 
					 "avatar" => $row[4], "created" => $row[5], "editted" => $row[6], "visible" => $row[7], "text" => $row[8]));
		mysqli_free_result($result);
		$query = "SELECT COUNT(id) FROM `comments` WHERE `comments`.`bid` = '{$id}' {$invisible};";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(13);
		$row = mysqli_fetch_row($result);
		if($row[0] - $start > 0)
			$return_array[0] = $row[0];
		mysqli_free_result($result);
		return $return_array;
	}
	$return_array = array("0");
}

function db_sendcomment($db_handle, $untrusted_bid, $untrusted_uid, $untrusted_commenttext)
{
	if(is_numeric($untrusted_bid) && is_numeric($untrusted_uid) && is_string($untrusted_commenttext))
	{
		$trusted_bid = (int) $untrusted_bid;
		$trusted_uid = (int) $untrusted_uid;
		$trusted_text = mysqli_escape_string($db_handle, trim($untrusted_commenttext));
		if($trusted_text === "")
			return 0;
		$query = "INSERT INTO `comments` SET `bid`='{$trusted_bid}', `user`='{$trusted_uid}', `text`='{$trusted_text}';";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(14);
		if(mysqli_affected_rows($db_handle) != 1)
			fatalerror(14);
		$query = "UPDATE `bounties` SET `comments`=`comments`+1 WHERE `id`='{$trusted_bid}';";
		$result = mysqli_query($db_handle, $query);
		if(!$result)
			fatalerror(15);
		return 1;
	}
	return 0;
}
?>
