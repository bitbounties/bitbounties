<?php
//
// common.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
include_once 'bb_config.php';
require_once 'header.php';
require_once 'footer.php';
define('BB_ERRORS_COMMENT_GENERAL', 1001);
define('BB_ERRORS_COMMENT_EMPTY', 1002);

function bb_maintenance()
{
	return BB_MAINTENANCE;
}

function bb_security_maintenance()
{
	return BB_SECURITY_MAINTENANCE;
}

function bb_register_enabled()
{
	return BB_REGISTERS;
}

function get_comments_perpage()
{
	return BB_COMMENTPERPAGE;
}
function get_bounties_perpage()
{
	return BB_BOUNTIESPERPAGE;
}
function get_pms_perpage()
{
	return BB_PMPERPAGE;
}
function bb_init()
{
	if(!defined('BB_MAINTENANCE'))
		fatalerror(999);
	elseif(bb_security_maintenance())
		fatalerrror(1000);
	elseif (bb_maintenance())
		fatalerror(0);
	session_start();
}
function btcpng()
{
	return '<img class="money" src="res/btc.png" alt="Ƀ" /> ';
}
function ltcpng()
{
	return '<img class="money" src="res/ltc.png" alt="Ł" /> ';
}

function formattime($time)
{
	return date("d M Y g:i:sA", strtotime($time));
}
function formatbtc($amount)
{
	return '<div class="money">' . btcpng() . money_format("%!^.8i", $amount / 100000000) . '</div>';
}
function formatltc($amount)
{
	return ltcpng() . money_format("%!^.8i", $amount / 100000000);
}
function fatalerror($id)
{
	header("Location: error.php?id=" . (int)$id);
	exit;
}
function lastrow_replace($needle, $replacement, $haystack)
{
	$pos = strrpos($haystack, "<tr>");
	if($pos === false)
		return $haystack;
	$haystack_array = str_split($haystack, $pos);
	$pos = strpos($haystack_array[1], $needle);
	if ($pos === false)
		return $haystack;
    $haystack_array[1] = substr_replace($haystack_array[1], $replacement, $pos, strlen($needle));
	return $haystack_array[0] . $haystack_array[1];
}
function lastrow_rreplace($needle, $replacement, $haystack)
{
	$pos = strrpos($haystack, "<tr>");
	if($pos === false)
		return $haystack;
	$haystack_array = str_split($haystack, $pos);
	$pos = strrpos($haystack_array[1], $needle);
	if ($pos === false)
		return $haystack;
    $haystack_array[1] = substr_replace($haystack_array[1], $replacement, $pos, strlen($needle));
	return $haystack_array[0] . $haystack_array[1];
}
function docroot()
{
	return $_SERVER['DOCUMENT_ROOT'];
}
?>
