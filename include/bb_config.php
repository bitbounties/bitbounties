<?php
//
// bb_config.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
define("BB_DB_NAME","bitbountydb");
define("BB_DB_USER","bb_user");
define("BB_DB_PASS","1");

define("BB_MAINTENANCE", 0);
define("BB_SECURITY_MAINTENANCE", 0);
define("BB_REGISTERS", 0);

define("BB_LTCBTC", 0.01);

define("BB_COMMENTPERPAGE", 10);
define("BB_BOUNTIESPERPAGE", 25);
define("BB_PMPERPAGE", 50);
?>
