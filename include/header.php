<?php
//
// header.php
// Copyright (C) 2016 bitbounties.org
//
// This file is part of bitbounties
//
// bitbounties is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// bitbounties is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitbounties.  If not, see <http://www.gnu.org/licenses/>.
//
require_once 'include/auth.php';

function display_header($user_array, $bb_page = "")
{
	if($user_array['uid'] >= 1 && ($bb_page === "login" || $bb_page === "register"))
		header("Location: index.php");
	$title = "bitbounties";
	$nav = create_header_navlinks($user_array, $bb_page);

	$output = "<!doctype html>";
	$output .= "<html><head><title>{$title}</title>";
	$output .= '<meta charset="UTF-8" /><link href="res/layout.css" rel="stylesheet" type="text/css" />';
	$output .= '</head><body>';
	$output .= $nav;
	echo $output;
}

function create_header_navlinks($user_array, $bb_page)
{
	$return = '<nav class="head-nav"><ul class="head-nav">' .
			'<li class="nav"><a class="nav" href="index.php">Home</a></li>' .
			'<li class="nav"><a class="nav" href="browse.php">Bounties</a></li>' .
			'<li class="nav"><a class="nav" href="new.php">New</a></li>' .
			'<ul style="float: right; list-style-type: none;">';

	if($user_array['uid'] >= 1)
	{
		$return .= '<li class="nav"><a class="nav" href="profile.php">' . $user_array['username'] . '</a></li>';
		if($user_array['alerts'] > 0)
			$return .= '<li class="nav"><a class="nav" href="inbox.php" id="alert">inbox: ' .
				(int)$user_array['alerts'] . '</a></li>';
		else
			$return .= '<li class="nav"><a class="nav" href="inbox.php">inbox</a></li>';
		$return .= '<li class="nav"><a class="nav" href="login.php?logout=1">logout</a>';
		$return .= '<li class="nav"><div class="balance">' . formatbtc($user_array['satoshi']) .
					formatltc($user_array['litoshi']) .'</div></li>';
	}
	else
	{
		$return .= '<li class="nav"><a class="nav" href="login.php">login</a>';
		if(bb_register_enabled())
			$return .= '<li class="nav"><a class="nav" href="register.php">register</a>';
	}

	$return .= '</ul></ul></nav>';
	switch($bb_page)
	{
		case "index":
			$return = str_replace('href="index.php">', 'href="index.php" id="active">', $return);
			break;
		case "browse":
			$return = str_replace('href="browse.php">', 'href="browse.php" id="active">', $return);
			break;
		case "new":
			$return = str_replace('href="new.php">', 'href="new.php" id="active">', $return);
			break;
		case "login":
			$return = str_replace('href="login.php">', 'href="login.php" id="active">', $return);
			break;
		case "register":
			$return = str_replace('href="register.php">', 'href="register.php" id="active">', $return);
			break;
		case "profile":
			$return = str_replace('href="profile.php">', 'href="profile.php" id="active">', $return);
			break;
		case "inbox":
			$return = str_replace('href="inbox.php">', 'href="inbox.php" id="active">', $return);
			break;
		default:
			break;

	}
	return $return;
}
?>
